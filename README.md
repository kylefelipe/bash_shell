[![pipeline status](https://gitlab.com/kylefelipe/bash_shell/badges/site-prod/pipeline.svg)](https://gitlab.com/kylefelipe/bash_shell/-/commits/site-prod)
<h1 align="center">Linux, Bash & Shell Script</h1>

![Divulgação](docs/img/bash_shell.png)
Esse é um "curso", um guia básico do que venho estudado sobre o assunto, e gostaria de compartilhar com os colegas.  
A idéia surgiu com o curso da Trybe, onde pude ver muitas coisas que eu já usava no dia a dia, por já usar Linux, e coisas novas.

## Descrição

Aqui irei abordar umas idéias básicas sobre o assunto e ir aprofundando na medida do possível.  
Ainda há um bom trabalho a ser feito, e na medida do possível irei atualizando. 
Irei também liberar alguns vídeos fazendo demonstrações do conteúdo, e na medida que forem sendo criados irei _linkando_ eles nos conteúdos.

## Capítulos

- [Uma gentil introdução ao Linux](docs/index.md)
  Uma introdução básica ao sistema operacional tão amado.

- [Terminal](docs/curso/terminal.md)
  Aqui irei falar um pouco sobre o "assustador" terminal, e será o início da demonstração que ele não passa de um _filhotinho fofo_, tipo um gatinho, cachorrinho, quem sabe, uma chinchila (Ah que saudade das minhas).

- [Trabalhando com diretórios](docs/curso/001-diretorios/README.md)
  Os primeiros passos na criação e navegação de diretórios.

- [Trabalhando com Arquivos](docs/curso/002-arquivos/README.md)
  Que tal saber manipular arquivos, criar novos, exibir o conteúdo, pesquisar palavras, chega aqui.

- [GREP - Global Regular Expression](docs/curso/002-arquivos/006_global_regex.md)
  Esse aqui é um camarada muito legal, é um poderoso recurso que utiliza Expressões Regulares para pesquisarmos dados e etc.  
  Não sabe o que é uma expressão regular? Então cola junto.

- [Manipulando Entrada e Saída](docs/curso/003-manipulando-io/README.md)
  Bom, seria bacana saber abrir um arquivo e redirecionar os dados dele para algum lugar né?  
  Ou pegar a saída que um programa gera e colocar dentro de um arquivo. Aqui é o lugar.

- [Pesquisando](docs/curso/004-metodos-pesquisa/README.md)
  Bom... Não vamos descobrir a cura de doneças terríveis, mas podemos encontrar aquele arquivo que salvamos com nossas letras de músicas preferidas para arrasarmos no caraokê quando pudermos fazer aquela festa de reencontro dos amigos, não é mesmo. Se bem que isso ajuda a curar muita coisa.

- [Fontes](docs/curso/fontes.md)
  Pois referência importa muito, não é mesmo...

Se ver algum [TODO] é que alguma coisa precisa ser criada ou melhorada, e contribuições serão bem-vindas.
