# LINUX BASH & SHELL

![Divulgação](img/bash_shell.png)

A intensão é entender como funciona o LINUX e o BASH SHELL e fazer scripts melhores.  
Não é obrigatório usar os mesmos nomes de pastas e aquivos, maaaas, tem de prestar atenção para não fazer besteira O.K?

## LINUX

O LINUX é um kernel, criado por volta de 1980, e devido sua liberdade, atraiu a atenção do criador do projeto GNU (GNU is not UNIX), Richard Stallman, que estava à procura de um bom kernel para utilizar em seu projeto, que apresentava ferramentas semelhantes aos que haviam nos sistemas [UNIX](https://pt.wikipedia.org/wiki/Unix), só que livres também, em contraponto a sistemas UNIX, que era proprietário e não tinha o código aberto na época.  
Devido a liberdade que oferecia aos usuários, e as ferramentas disponíveis nesse casamento ente Linux e GNU, essa dupla dinâmica começou a ser utilizada em servidores e pcs, pois ao ser instalado, os usuários já podiam contar com uma série de ferramentas prontas para usar. Possui também uma série de opções de interface gráfica para o usuário (GUI - Graphical User Interface), porém conhecer como ele funciona via interface de linha de comando (CLI - Command Line Interface) é essencial, pois a GUI nem sempre consegue cobrir 100% das funcionalidades do sistema.

Ele possui várias versões, sendo as principais:

- Debian;
- Ubuntu (Baseado em Debian);
- Arch;
- Mint (Baseado em Ubuntu);
- Red Hat (Voltado para servidores);
- CentOs (voltado para servidores, uma versão do Red Hat feita pela comunidade);
- Kali;
- Android.

Já o UNIX está disponível nas seguintes versões:

- MAC OS, basicamente está presente em todos os dispositivos Apple;
- OpenBSD;
- FreeBSD;

Aqui vou usar GNU/Linux - Debian, que foi o que melhor me adaptei, mas os comandos não devem mudar muito de um LINUX.

## Usuários

No LINUX temos pelo menos dois usuários:

O usuário comum, criado no momento da instalação do sistema ou depois, quando é necessário ter outras pessoas acessando à maquina. programas também podem ter usuários próprios, o PostgreSQL é um desses casos.
as permissões de usuários comuns são limitadas, e cada um tem sua própria pasta no sistema.

E o ROOT, esse usuário não é um *usuário nutella*, é um usuário raiz, tem super poderes, grandes responsabilidades, administra o sistema, e manda nos outros usuários, se o ROOT proibir um usuário de acessar um arquivo somente ele poderá dar a permissão de volta.

> "Com grandes poderes vem grandes responsabilidades" PARKER, Ben

## Dissecando o LINUX

O sistema é dividio em três partes:

- Kernel: Núcleo, é responsável pelas funções, trabalhar os recursos da máquina;
- Shell: Ela é a forma de iteração entre o usuário (ou usuários, podem ser mais de um) e o sistema.
- Os Aplicativos: a maiora dos comandos são tidos como aplicativos (ou programass) dentro do [UNIX](https://pt.wikipedia.org/wiki/Unix), e o usário ainda pode instalar novos programas e criar seus próprios para extenderem as funcionalidades do S.O. (Sistema Operacional).

No Linux as coisas funcionam da seguinte forma:

Ou são arquivos, ou são processos.

Processos são programas em execução, e possuem um PID (Process IDentifier) único para cada um.
Já os arquivos... bom.. acho que eu não preciso falar sobre né....

## Algumas regrinhas básicas

Caracteres especiais e acentos e espaços, evite de usar em nomes de objetos (como pastas, arquivos e variáveis). O "_" está liberado.  
O espaço é utilizado para separar opções e parametros no terminas.

O ponto (`.`).  
Este é um caractere especial no nosso lindo sistema.  
Quando utilizado no inicio do nome de pastas e arquivos o sistema o interpreta como arquivos ou pastas ocultos.

Quando utilizado sozinho significa 'diretório atual'.  
Por exemplo:  
Se estiver dentro de Documentos, e usar um comando que precise de um diretorio, você pode utilizar o . como argumento.  
Quando usamos ele dobrado (`..`) significa 'a pasta anterior'.  
A utilização do ponto vai ficar bem clara nos capitulos a seguir.

O cifrão (`$`), ele é utilizado para chamarmos uma variável ou um parâmetro dentro de um script.  
Por exemplo: `echo $PATH` mostra todos os endereços dentro da variável de ambiente `PATH`.

[TODO: Falar um pouco mais de cada um dos tópicos do dissecando o LINUX]  
[TODO: Falar do sistema de arquivos]
