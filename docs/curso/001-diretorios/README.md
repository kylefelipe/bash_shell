# Trabalhando com DIRETÓRIOS

Umas das coisas que mais vamos precisar saber é como trabalhar com diretórios dentro do terminal.  
Precisamos saber listar arquivos e sub diretórios, visualizar dados ocultos, permissões de arquivos e pastas, criar novos, portanto, vamos começar por eles primero.

## *L*i*S*tando

Isso mesmo, eu coloquei dessa forma para fixar bem o primeiro comando, o **ls**, ele lista tudo que tem dentro de um diretório.  
Vá no terminal e digite:

```shell
ls # pressione enter
> Documentos  Imagens  Musica  Videos  模板
```

O comando irá mostrar o conteúdo da pasta em que o usuário estiver. Também podemos passar um caminho de um diretorio completo, ou relativo,  como parâmetro para ele:  

```shell
ls Documentos/
> arq1.txt arq2.txt pasta1 pasta2
ls Documentos/pasta1
> #pasta vazia
```

Pronto, é isso.

Só que não, ainda temos várias opções que podemos usar no comando ls.

### Opções do comando ls

As opções dos programas alteram o comportamento deles, podendo exibir mais ou menos informações, ou exibindo de uma forma diferente.  
Não vou tratar sobre todas, apenas as mais usadas, mas pode usar e abusar da primeira opção a seguir.

#### Buscando Ajuda

* `--help`

O mais importante de todas as opções é o `--help` essa opção mostra ajuda do programa, uma mão na roda quando estamos em dúvida do que cada opção faz, ou não sabemos como usar o programa. Todo bom programa CLI tem essa opção.

```shell
ls --help
```

![saida do ls --help](../../img/ls-2.png)

O rei, Roberto Carlos diria:

> -São tantas opções!

#### Listando tudo

* `--all`
* `-a`

O essa opção lista tudo dentro de um diretório, e não ignora entradas que iniciam com o `.`.  

![saida]() #colocar aqui a imagem da saida do comando

Verão que que algumas saídas são coloridas, essas em azul são pastas.

#### Lista Completa

* `-l`

Essa opção mostra uma lista completa de cada arquivo e pasta no diretório.  
Como permissões de acesso data e horário da ultima modificação.

![saida]() #colocar aqui a imagem da saida do comando

Podemos perceber que no inicio de cada linha aparece uma sequência de 10 caracteres:

`-rw-r--r--`

O primeiro caractere indica o tipo que representa entrada:

`*-*rw-r--r--`

* `-` a entrada é um arquivo comum;
* `d` a entrada é um diretório;
* `l` a entrada é um link simbólico.

A próxima sequência caracteres dizem respeito às permissões de utilização, são três grupos de três caracteres, e eles podem ser os seguintes.  

* `r` Permissão de leitura;
* `w` Permissão de escrita;
* `x` Permissão de execussão.

A ordem em que os caracteres aparecem serão sempre essa:

`rwx`

O caractere `-` indica que a permissão foi negada.

> Mas você falou em três grupos de caracteres!

Bom, os grupos de caracteres são referentes a:

`usuário grupo outros`

Sempre nessa mesma ordem, isso ficará claro mais a frente.

Logo após temos um número que é um link rigido, que é nome adicional para um arquivo existente.

Os dois nomes que vem logo em segida representam o usuário proprietário do arquivo e o proprietário do grupo de usuários ao qual o proprietário pertence.

O valor seguinte é o tamanho do arquivo.  
A data e hora que vem logo em seguida são referentes à ultima modificação que ocorreu.  
Por último vem o nome do arquivo junto com a extensão.

Vamos aprender a criar uns diretórios.

## Criando Diretórios

Criação de diretórios são muito fáceis no linux, basta usar o **mkdir** (_MaKe DIRectory)

```shell
$ mkdir meu_primeiro_diretorio
```

O comando irá criar a pasta passada no argumento.  
Podemos passar nome de mais de uma pasta separada por espaço, elas serão criadas dentro do diretório.

```shell
$ mkdir pasta1 pasta2 pasta3
ls
```

![Pastas Criadas](../../img/mkdir_001.png)

### Opções do comando mkdir

O comando `mkdir` possui algumas opções a serem utilizadas

#### Pedindo ajuda

* `--help`

Esse comando mostra o help do comando.

#### Criação de subpastas

* `--parents`
* `-p`

Não gera erro caso a pasta já exista.

No linux, a separação de pastas/arquivos é feita pelo caractere `/`, se encadearmos uma sequência de nomes separados por esse caractere ele irá criar as pastas que não existirem dentro do caminho indicado.

```shell
$ mkdir -p pasta1/subpasta1/outrasubpasta1
$ ls pasta1
> subpasta1
$ ls pasta1/subpasta1
> outrasubpasta1
```

![Criando subpastas](../../img/mkdir_002.png)

## Removendo diretórios

Sabe aquele tanto de pasta que criou para o fazer esse curso??  
Pode apagar todos eles usando o comando `rmdir` (ReMove DIRectory), se estiverem vazios.  

```shell
cd ~/Documentos
$ ls
> arq1  arq_pelago  meu_primeiro_diretorio  pasta1  pasta2  pasta3
$ rmdir pasta3
```

![Removendo Pasta](../../img/rmdir_001.png)

### Buscando ajuda

* `--help`

Mostra ajuda do comando.

### Ignorando erros

* `--ignore-fail-on-non-empty`

Ignora o erro quando a pasta não estiver vazia, e mesmo assim não remove ela.

```shell
$ rmdir pasta1
> rmdir: falhou em remover 'pasta1': Diretório não vazio
$ rmdir --ignore-fail-on-non-empty pasta1
ls
> arq1  arq_pelago  meu_primeiro_diretorio  pasta1  pasta2
```

![Ignorando o erro de Diretório não vazio](../../img/rmdir_002.png)

## Mudando de diretório

Uma opção muito importante que usamos o tempo todo é o de mudar de diretório (Change Directory).
Com ele podemos mudar para um determinado endereço para trabalharmos lá dentro, criando arquivos, procurando outras pastas, e por ai vai.
Basta passar um endereço, absoluto ou relativo, que iremos para a pasta

![Mudando de Pasta](../../img/change_dir_001.png)

Podemos passar os nomes das pastas encadeados, separados por `/` para acessar subpastas mais profundas.

![Acessando pastas mais profundas](../../img/change_dir_002.png)

Lembra que na introdução eu disse sobre o `..` pois é, aqui fica bem claro a utilização dele. Usando ele podemos sair da pasta que estamos trabalhando, e ir para uma anterior.

![Mudando de Pasta](../../img/change_dir_003.png)

Podemos usar sequências de `..` separadas por `/` para irmos para pastas superiores de uma vez.

![Mudando de Pasta](../../img/change_dir_004.png)

O `~` representa a pasta do usuário, é onde fica todas as informações e arquivos gerados pelos usuários, e fica dentro da pasta HOME.

![Pasta do usuário](../../img/change_dir_005.png)

Acessando uma pasta a partir da pasta do usuário.

![Pasta do usuário](../../img/change_dir_006.png)

Caso queira voltar para a última pasta assessada, basta passar como parâmetro o `-`

![Pasta do usuário](../../img/change_dir_007.png)

### Buscando Ajuda

`-- help`

Mostra a ajuda do comando.

## Mostrando o caminho do diretório atual

Um comando muito útil que vimos agora a pouco foi o `Print Working Directory` - pwd.
Ele simplesmente retorna na tela o diretório que estamos trabalhando.

![Pasta do usuário](../../img/change_dir_008.png)

### Buscando ajuda

`--help`

Exibe a ajuda do comando na tela.
