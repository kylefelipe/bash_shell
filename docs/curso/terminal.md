# Terminal

Aqui vamos ver os comandos que podemos utilizar no temrial [BASH](https://pt.wikipedia.org/wiki/Bash).
É possível instalar ele no Windows, basta uma *Googlada* sobre, [clique aqui](https://www.techtudo.com.br/dicas-e-tutoriais/noticia/2016/04/como-instalar-e-usar-o-shell-bash-do-linux-no-windows-10.html).

Há uma variedade muito grande de SHELLs no mercado (BASH, FISH, ZSH e por aí vai), mas vou concentrar esforços no [BASH](https://pt.wikipedia.org/wiki/Bash) SHELL.

O terminal, a linda telinha preta, amada por muitos e amedrontante para outros (não deveria ser), é a forma de iteração nos sistemas que utilizam GUI, há muitos tbm (gnome-terminal, konsole...) mas o padrão que vier na sua instalação *já tá bão*.

É importante saber que os terminais identificam os tipos de usuários usando caracteres, que podem variar com a versão que está utilizando.  
Esses caracteres são vistos tanto no terminal, quanto nos sites, em tutoriais que são disponibilizados, e fazem referência a qual usuário deve executar o comando.

No linux são esses:

- **$** Indica que é um usuário comum.  
![usuário comun](../img/terminal_usuario_comum.png)

Portanto se vir um comando em algum tutorial no seguinte padrão:  

`$ comando a ser executado`

Indica que o comando será executado pelo usuário comum.

- **#** Indica que é o super usuário, ROOT.  
![usuário root](../img/terminal_usuario_root.png)

Se vir um comando no seguinte padrão:  

`# comando a ser executado`

O comando deverá ser executado como root.

A partir de agora vamos ver os programas que já vem instalados no sistema, e fazer parte da trama de dominar o mundo usando o terminal, ou não, só brincar mesmo com scrpts engraçados.  
O [BASH](https://pt.wikipedia.org/wiki/Bash), assim como outros SHELLs, é ***SENSÍVEL À CAIXA*** (Case Sensitive), portanto cuidado quando um comando estiver gerando um erro ou um resultado inesperado.

A partir de agora vamos começar a usar os comandos no terminal.  
Esses comandos são programas e scripts, eles podem receber opções e outros parâmetros para serem utilizados.  
Normalmente as opções são passadas utilizando caractere `--`, para opção completa, e `-` quando é oferecido um 'apelido' para a mesma opção.  
Não são todos os programas que seguem essa regra, mas não se preocupe, eu vou mostrar ambos os formatos.

Bora ver como trabalhar os diretórios
