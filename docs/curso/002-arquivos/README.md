# Trabalhando com arquivos

No Unix/linux quase tudo no sistema gira em torno de um arquivo, pode ser um script, arquivo de configuração ou até mesmo um arquivo de texto comum.  
O interessante é que normalmente os arquivos criados são de texto, mesmo quando não criados utilizando alguma extensão de arquivo.  
Existem muitas formas de criar arquivos, vamos ver uma forma principal aqui e as outras formas vamos ver no decorrer do curso.

## Criando um arquivo

Uma forma muito simples de criar um arquivo vazio via terminal é o comando `touch`, ele é utilizado para atualizar a data de hora que um arquivo foi acessado e modificado para o horário que o comando foi executado. Caso o arquivo não exista, o touch cria ele.

```shell
cd Documentos
$ touch arq1
$ ls
> arq1  meu_primeiro_diretorio  pasta1  pasta2
```

![Criando um arquivo com Touch](../../img/touch_001.png)

Bom, lembra da função principal do `touch`, vamos testar aqui.

```shell
$ ls -l aqr1
> -rw-r--r-- 1 cursobash cursobash 0 mai  1 22:32 arq1
$ touch arq1
$ ls -l arq1
> -rw-r--r-- 1 cursobash cursobash 0 mai  2 12:14 arq1
```

![Tocando o arquivo](../../img/touch_002.png)

Veja que a data no primeiro `ls` era primeiro de Maio 22:32, e no segundo 2 de Maio 12:14.

Podemos passaar mais de um arquivo, separado por espaços, para criar mais de um arquivo.

```shell
$ touch arq2 arq3
$ ls
```

![Criando mais de um arquivo](../../img/touch_003.png)

Se passarmos o endereco de um arquivo, o touch vai criar ele dentro da pasta. Só tome cuidado com caminhos absolutos e relativos.

```shell
touch pasta1/arq1
ls pasta1
> arq1  subpasta1
```

### Obtendo ajuda

* `--help`

Mostra a ajuda do comando.

### Não quero criar o arquivo

* `--no-create`
* `-n`

Esse comando não cria o arquivo caso não exista.

## Copiando Arquivos

Em muitas situações precisamos copiar arquivos, duplicar e é isso que o comando `cp` (CoPy) faz. Passamos para ele 2 parametros:

* Caminho do arquivo a ser copiado;
* Caminho para onde queremos copiar o arquivo;

```shell
$ cp arq2 pasta2
$ cd pasta2
$ ls
> arq2
```

![Copiando arquivos](../../img/copiando_001.png)

Vamos copiar o arq3 para dentro da pasta2, mas não precisamos sair dela para isso.

```shell
$ cp ../arq3 .
$ ls
> arq2  arq3
```

![Copiando Arquivo da pasta anterior](../../img/copiando_002.png)

Viu como é útil a utilização do `.` e do `..`?

podemos fazer backup de um arquivo também usando o o `cp`, especificando o um novo nome para o arquivo.

```shell
$ cp arq3 bkp_arq3
$ ls
> arq2  arq3  bkp_arq3
```

![Fazendo Bkp](../../img/copiando_003.png)

### Obtendo Ajuda

* `--help`

Mostra a ajuda do programa.

### Caso o arquivo já exista

* `--interactive`
* `-i`

Essa opção pergunta se deseja sobreescrever o arquivo, caso exista.

* `--no-clobber`
* `-n`

Não sobreescreve o arquivo, se essa opção for usada junto com a opção `-i`, o `-n` prevalece.

## Movendo arquivos

Em alguns momentos queremos mover um arquivo de uma pasta para outra sem fazer uma nova cópia do mesmo, essa é a função do `mv` (MoVe).

Volte para a pasta Documentos, teremos dentro dela 3 arquivos e 4 pastas. vamos mover o arq2 para dentro da pasta1.

```shell
$ mv arq2 pasta1
$ ls pasta1
> arq1  arq2  subpasta1
$ ls
> arq1  arq3  meu_primeiro_diretorio  pasta1  pasta2  pasta3
```

![Movendo arquivo](../../img/movendo_001.png)

O comando MV também é utilizado para renomear arquivos.

```shell
$ ls
> arq1  arq3  meu_primeiro_diretorio  pasta1  pasta2  pasta3
$ mv arq3 arq_pelago
$ ls
> arq1  arq_pelago  meu_primeiro_diretorio  pasta1  pasta2  pasta3
```

![Renomeando Arquivo](../../img/movendo_002.png)

### Obtendo Ajuda

* `--help`

Mostra o help do programa.

### Caso o arquivo já exista

* `--interactive`
* `-i`

Essa opção pergunta se deseja sobreescrever o arquivo, caso exista.

* `--no-clobber`
* `-n`

Não sobreescreve o arquivo, se essa opção for usada junto com a opção `-i`, o `-n` prevalece.

## Removendo arquivos

Criou arquivo errado, como nome errado? ou já não precisa mais das várias versões finais de uma poesia escreveu?  
Basta apagar os arquivos.

O comando `rm` (ReMove) será o nosso aliado na hora de remover arquivos e pastas indesejadas.

```shell
$ rm arq1
$ ls
> arq_pelago  meu_primeiro_diretorio  pasta1  pasta2
```

![Removendo Arquivo](../../img/remove_001.png)

### Obtendo Ajuda

* `--help`

Mostra a ajuda do comando.

### Controlando a remoção

* `-i`

Pergunta se deseja remover antes de cada remoção.

* `-I`

Questiona apenas uma vez antes de remover (acima de três arquivos ou fazer remoção recursiva)

### Remoção recursiva

* `--recursive`
* `-R`
* `-r`

Essa opção remove recursivamente, pastas e seus conteúdos.

```shell
$ ls
>arq_pelago  meu_primeiro_diretorio  pasta1  pasta2
$ rm pasta2
>rm: não foi possível remover 'pasta2': É um diretório
$ rm -r pasta2
$ ls
>arq_pelago  meu_primeiro_diretorio  pasta1
```

![Remoção Recursiva](../../img/remove_002.png)

## Limpando a Tela

Como vamos usar arquivos, precisamos saber trabalhar seu conteúdo, inserir, exibir, filtrar informações dentro dele. E haja programas para se fazer isso nativamente, são muitos.

Mas nossa tela deve estar muito cheia de coisas, muito poluída. 
Para isso temos duas formas:

* `clear`

O comando irá limpar a tela, sem a possibilidade de ver o que já foi executado anteriormente, bem como algum retorno que o comando tenha gerado.

### Buscando ajuda

* `--help`

Pegadinha... ele não possui essa opção e gera um erro, MAAASSSS... ele mostra na mensagem de erro uma forma de utilização do programa e algumas opções básicas.  

### Limpando a tela sem perder o anterior

* `-x`

Essa opção limpa a tela e permite rolar a tela para ver comandos anteriores e algum retorno que eles tenham gerado.

E a tecla de atalho de atalho `CTRL+l` que tem o mesmo efeito do `clear -x`

Faça o teste ai com todas essas opções. Se quiser use o `grep --help` a ajuda dele é bem grandinha e vai ocupar bem o terminal.  
O `grep` será visto mais a frente.

Para essa parte, eu fui no site [letras*](https://www.letras.mus.br) e escolhi uma letra de um dos meus cantores favoritos, [Vander Lee](https://www.letras.mus.br/vander-lee/), e escolhi umas das minhas músicas favoritas dele, [Eu e Ela](https://www.letras.mus.br/vander-lee/1449852/) e salvei em um arquivo `.txt` dentro de `Documentos`. Vou usar ele nos exemplos, mas pode ficar a vontade para escolher qualquer arquivo de texto que quiser.

## Exibição de conteúdo

Existem várias formas de exibir o conteúdo de um arquivo de um arquivo no terminal, cada um exibe de uma forma diferente e possui funções diferentes.

## Concatenar

O comando `cat` (conCATenate) tem a função de unir arquivos e, também, exibir seu conteúdo na tela.

```shell
$ cat eu_e_ela.txt
```

![Exibindo conteúdo](../../img/concatenate_001.png)

Ele vai exibir todo o seu conteúdo na tela, é bem indicado para exibir arquivos com poucas linhas.

### Obtendo ajuda

* `--help`

Exibe a ajuda do comando.

### Numerando linhas

* `-number`
* `-n`

Essa opção enumera todas as linhas de um documento, inclusive as linhas em branco.

```shell
cat -n eu_e_ela.txt
```

![Numerando as linhas](../../img/concatenate_002.png)

* `--number-nonblank`
* `-b`

Essa opção enumera todas as linhas não vazias do arquivo de texto.

```shell
$ cat -b eu_e_ela.txt
```

![Numerando linhas não vazias ](../../img/concatenate_003.png)

### Tratando linhas em branco

* `--squeeze-blank`
* `-s`

Essa opção suprime as linhas em branco.

#### Tratando caracteres não imprimíveis

* `--show-ends`
* `-E`

Exibe o cifrão (`$`) no final de cada linha

* `--show-tabs`
* `-T`

Exibe os caracteres de tabulação como o `^I`.

Eu coloquei uma tabulação no final do arquivo `eu_e_ela.txt`.

```shell
$ cat -T eu_e_ela.txt
#>^Itabulado # trocou a tabulação pelo caractere ^I
```

![Mostrando caratere de Tabulação](../../img/concatenate_004.png)

* `--show-nonprinting`
* `-v`

Utiliza uma notação diferente para caracteres especiais (^ e M-, exceto para o TAB e LFD)

* `-t`

É a união dos comandos `-v` e `-T` (`-vT`)

* `-all`
* `-a`

Tudo junto e misturado.

## Cabeçalho

Quando o arquivo é muito grande podemos imprimir apenas uma parte, o comando `head` (cabeça) por padrão mostra as 10 primeiras linhas de um arquivo, na verdade, as 10 primeiras linhas de uma entrada passada para ele.

```shell
$ head eu_e_ela.txt
```

![Mostrando inicio do conteúdo](../../img/head_001.png)

### Obtendo ajuda

* `--help`

### Exibindo quantidade de linhas diferentes

* `--lines=[-]numero`
* `-n`

Exibe a quantidade de linhas informadas. se for utilizado um número negativo (`-numero`) será exibido todas as linhas menos as últimas n linhas informadas.

```shell
$ head -n 5 eu_e_ela.txt
$ head --lines=5 eu_e_ela.txt
```

![Modificando quantidade de linhas exibidas](../../img/head_002.png)

## Final de arquivo

O comando `tail` mostra, por padrão, as 10 últimas linhas de um arquivo, ou a cauda (no inglês tail).

```shell
$ tail eu_e_ela.txt
```

![Exibindo o final de um arquivo.](../../img/tail_001.png)

### Obtendo ajuda

* `--help`

Exibe a ajuda do comando.

### Exibindo quantidade de linhas diferentes

* `--lines=[+]numero`
* `-n`

Exibe as últimas linhas informadas no comando. Caso o número for informado com o `+` na frente, exibe as linhas a partir do número informado.

```shell
$ tail -n 3 eu_e_ela.txt
$ tail -n +50 eu_e_ela.txt
$ tail --lines=+50 eu_e_ela.txt
```

![Quantidade de linhas diferentes](../../img/tail_002.png)

## Paginando o conteúdo

Se o arquivo for muito grande, e queremos ver todo ele, o melhor é paginar a saida do arquivo, é para isso que serve o comando `less`.

```shell
$ less eu_e_ela.txt
```

![Paginando Conteúdo](../../img/less_001.png)

Ele é bem diferente dos comandos que usamos até agora, pois podemos usar o teclado para navegarmos no texto e fazer pesquisas dentro do mesmo, para sair, o normal é digitar `q`.

### Navegando no arquivo

Para esses caso seja informado um número N, esse número será utilizado na rolagem, o padrão é 1

* `e` Rola o texto N linhas para frente.
* `y` Rola o texto N linhas para trás.
* `espaço`, `f`, Rola o texto página por página para a frente ou N Lihas.
* `ESC-v`  Rola o texto página por página para trás ou N linhas.

### Pesquisando no arquivo

Também é possível pesquisar dentro de um arquivo e navegar por essa pesquisa.

* `/padrão` Pesquisa o padrão informado a partir da posição pra frente no arquivo.
* `?padrão` Pesquisa o padtão informado a partir da posição para trás no arquivo.
* `n` Repete a última pesquisa até a enésima ocorrência.
* `N` O mesmo que `n` só que na ordem reversa.
* `esc-u` Liga/desliga a ênfase das ocorrências que coincidirem com o padrão.
* `&padrão` Exibe apenas as linhas que coincidirem com o padrão.

### Obtendo ajuda

* `--help`
* `-?`

Mostra a ajuda do comando.

## Brincando de Contar

Temos um programa especial para contar palavras dentro de um arquivo ou uma entrada do tipo texto, e ele entende por aquivo um conjunto de caracteres delimitados por espaço em branco.  
Essa é a função do comando `wc` (Word Count, e não Water Closet, ou banheiro, em pt_Br).  
O comando sem parâmetro nenhum retorna:

Quantidade de linhas, palavras o tamanho em bytes do arquivo e o nome do arquivo.

```shell
$ wc eu_e_ela.txt
> 53  263 1206 eu_e_ela.txt
```

![Contando](../../img/contando_001.png)

### Buscando ajuda

* `--help`

Exibe a ajuda do comando.

### Contando linhas

* `--linhas`
* `-l`

Exibe a quantidade de linhas.

### Contando caracteres

* `--chars`
* `-m`

Conta os caracteres.

### Contando palavras

* `--words`
* `-w`

Conta as palavras.

### Tamanho da linha mais longa

* `--max-line-length`
* `-L`

Mostra a quantidade de caracteres da linha mais longa.
