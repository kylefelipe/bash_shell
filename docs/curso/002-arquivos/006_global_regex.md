# Global Regular Expression Print

Esse camarada merece um curso inteiro à parte.

Durante esse tempo que tenho aprofundado mais na área de T.I me deparei com uma ferramenta muito, mas muito boa de se utilizar.  
Ela se chama **EXPRESSÃO REGULAR** (**REGEX**), ela foi criada no intuito de procurar padrões em textos, a história da sua criação é muito boa de se ler (procure pelo livro `[EXPRESSÕES REGULARES]` do [Aurélio Marinho Jargas](https://aurelio.net/)).  
Ela utiliza meta caracteres para padronizar a pesquisa, é algo bem bacana de estudar.  
A capacidade desse recurso é tão boa que diversos programas implementaram esse padrão.  
E um deles, um editor de texto via linha de comando, o (`ed`), possuia um comando `g` que aceitava REGEX (`RE`) para realizar pesquisas globais e um comando `p` (Print) que imprimia na tela.  
A sintaxe ficava `g/RE/p` (Global Regular Expression Print) e ele deu origem ao programa`grep`.  
Uma das funções desse programa é pesquisar palavras e padrões em arquivos de texto ou outras entradas de texto que forem passadas a ele.  

```shell
$ grep So eu_e_ela.txt
```

![Filtrando linhas](../../img/grep_001.png)

O comando mostra apenas as linhas onde foi encontrada a ocorrência de *So* mas, o programa considera a _caixa_ da palavra passada na busca.

### Buscando ajuda

* `--help`

Exibe a ajuda do comando.

### Ignorando a caixa alta

* `--ignore-case`
* `-i`

Essa opção não leva em consideração se as letras são maiúsculas ou minúsculas.

```shell
$ grep -i so eu_e_ela.txt
```

![Ignorando a caixa](../../img/grep_001.png)

### Invertendo a busca

* `--invert-match`
* `-v`

Seleciona as linhas onde o padrão não foi encontrado.

### Contando

* `--count`
* `-c`

Conta as linhas onde o padrão foi encontrado.

### Opções de comparação

* `--word-regexp`
* `-w`

Procura pela palavra completa.

* `--line-regexp`
* `-l`

Procura pela linha completa.

* `--regex=PADRÕES`
* `-e`

Aplica o padrão `REGEX` na busca.

Regex é um assunto (BEM|MUITO) extenso, e merece um tutorial inteiro só para ele.

> -Mas eu vi vc brincando com REGEX nessa última linha.  
> -Ahh, percebeu né?!

Eu fiz um vídeo para o canal [Geocast Brasil](https://www.youtube.com/geocastbrasil) falando sobre [expressões regulares](https://youtu.be/n6v8K1DoGxM), e super recomendo o livro do Jargas.
