# Manipulação de saida e entrada

No terminal podemos manipular a saida de comandos, podendo redirecionar para outro arquivo ou para outros programas trabalharem.  
E temos muitas opções para trabalhar isso.

## Redirecionando a saída

Podemos redirecionar a saída usando o comando `>`, ele substitui o arquivo inteiro com a saida de um comando. Ele também é utilizado para redirecionar a saida para a entrada de outro comando.  
O comando `cat` é também utilizado para escrever o texto para uma saida ou arquivo.  
É indicado para trabalhos simples, mas para mais complexos, é melhor usar um editor de texto.  
Ao terminar de inserir texto em um comando `cat` sempre termine com uma linha nova vazia e para finalizar pressione `Ctrl+d` (`^d`) na linha em branco.  
O <enter> vai para uma nova linha.

```shell
$ cat > arquivo_editado.txt #pressione enter
primeira linha #pressione enter
segunda linha #pressione enter
terceira linha #pressione enter
`^d`
$ ls
>arq_pelago           eu_e_ela.txt            pasta1
arquivo_editado.txt  meu_primeiro_diretorio
```

![Redirecionando a saida](../../img/manipula-io_001.png)

Tem que tomar cuidado, pois, caso o arquivo exista, ele será substituido.

```shell
$ cat arquivo_editado.txt
$ cat > arquivo_editado.txt
novo conteúdo do arquivo
^d
$ cat arquivo_editado.txt
>novo conteúdo do arquivo
```

![Substituindo conteúdo](../../img/manipula-io_002.png)

### Obtendo ajuda

Não tem, não é pegadinha.  
Esse comando não possui opções.

## Adicionando dado em um arquivo

Algumas vezes precisamos adicionar alguma saida ao final de um arquivo, para isso usamos o comando `>>`.  
Ele vai inserir uma saida para o final de um arquivo, ou outro programa que recebe entradas.

```shell
$ cat >> arquivo_editado.txt
nova linha inserida
^d
$ cat arquivo_editado.txt
>novo conteúdo do arquivo
>nova linha inserida
```

![Adicionando conteúdo](../../img/manipula-io_003.png)

### Obtendo ajuda

Também não...

## Juntando vários arquivos

Mais uma função do programa `cat`.  
Podemos passar mais de um aquivo para ele e redirecionar a saída um novo aquivo.

```shell
$ cat eu_e_ela.txt arquivo_editado.txt > tudo_junto.txt
$ tail -n 5 tudo_junto.txt
>Quando a noite me revela
>teste de tabulação
>	tabulado
>novo conteúdo do arquivo
>nova linha inserida
```

![Concatenando arquivos](../../img/manipula-io_004.png)

Olha aí, já estamos começando a usar os comandos que já vimos até aqui.

## Ordenando entradas

Já quis ordenar números, nomes ETC.? Temos o programa `sort`, ele é o resposável por ordenar uma lista passada para ele.

vamos criar uma pasta para não misturar as coisas e vamos criar uns aquivos lá dentro com listas.  
A primeira lista, será de nomes de cidades, e a segunda de países. Pode usar a forma que achar melhor para criar as listas.

```shell
$ mkdir listas
$ cd listas
$ cat > cidades
Belo Horizonte
Altamira
Vitória
Contagem
^d
$ cat > paises.txt
Brasil
México
Bolívia
Argentina
^d
$ sort paises.txt
$ sort cidades
```

![Ordenando listas](../../img/ordenando_001.png)

Preciso mostrar como montar um arquivo com o nome das cidades em ordem?  
Dica, não redirecione a ordenação de um arquivo para o mesmo arquivo...

Podemos usar o programa `<` para redirecionar a saida na direção inversa.

```shell
$ sort < cidades
```

![Redirecionando na direção inversa](../../img/ordenando_002.png)

Vai pegar o conteúdo de cidade e jogar como entada no `sort`.

### Obtendo ajuda

* `--help`

Exibe a ajuda do comando.

### Ignorando algumas coisas

* `--ignore-leading-blanks`
* `-b`

Ignora espaços em branco iniciais.

* `--ignore-case`
* `-f`

Não diferencia minúsculas de maiúsulas.

### Invertendo a ordem

* `--reverse`
* `-r`

Inverte a ordem.

### Usando números

* `--numeric-sort`
* `-n`

Ordena de acordo com um valor numérico.

## Brincando de Mário

Podemos manipular a saída de um programa para a entrada de outro usando pipe (cano, ou tubo).

> Aaahhhh entendi, brincar de Mário!

E para isso usamos a barra vertical `|` para isso, esse programa é bem mais rápido que o uso do `>`, `>>` e do `<`.

```shell
$ cat paises.txt cidades | sort > pais_cidade_ordenados.txt
$ cat pais_cidade_ordenados.txt
>Altamira
>Argentina
>Belo Horizonte
>Bolívia
>Brasil
>Contagem
>México
>Vitória
```

![Pipes](../../img/pipes_001.png)

Numa tacada só nos juntamos 2 arquivos, colocamos em ordem alfabética e direcionamos a saída para um arquivo.

[TODO: Uso do SED]
[TODO: Uso do AWK]