# Métodos de pesquisa

Pesquisar por conteúdos de pastas e arquivos é tarefa rotineira na vida de todo mundo.  
Quem nunca fez o download de um arquivo da internet e não esqueceu onde colocou, ou abriu vários arquivos de texto procurando por um conteúdo específico?

Pois é, pare para pensar no quanto de tempo teria economizado se tivessem te apresentado o BASH SHELL mais cedo.

## Cartas Selvagens

As `wildcards`, não, não estou falando da coleção de livros escrita por George R. R. Martin (achou que ele escreveu só Game of Thrones?!) que provavelmente devem ter sido o motivo dessa funcionalidade receber esse nome, são também muito utilizadas em pesquisas, quando usamos o `*.docx` para pesquisar um aquivo estamos usando elas.  
São meta caracteres (coringas) que nos permitem criar padrões no momento da pesquisa, principalmente quando não sabemos o nome completo de um arquivo ou pasta. É bem no estilo de [expressões regulares](../002-arquivos/006_global_regex.md).  
Também são conhecidas como `Globbin Patterns`, por causa de um programa do [UNIX](https://pt.wikipedia.org/wiki/Unix) que se chama `Glob`, digite no terminal `man glob` que irá ver o manual dele, pressione `q` para sair.
O interessante é alem de ser muito util em pesquisas nos permite executar ações em varios arquivos, pastas etc. sem ter de ficarmos especificando cada um deles.

Aqui vão algumas delas.

## Qualquer coisa

Podemos utilizar o `*` para representar qualquer sequência de caracteres. e sua posição no padrão influência em onde essa sequência pode aparecer.

```shell
# Listando todos os arquivos .txt de uma pasta
cd ~/Documentos # Somente para ir para a pasta
$ ls *.txt
```

![Listando arquivos de .txt](../../img/wildcards_001.png)

```shell
# Listando arquivos que tenham ar no inicio do nome
$ ls pas*
```

![Usando coringa no final](../../img/wildcards_002.png)

Executando o `touch` em todos os arquivos .txt:

```shell
$ ls -l *.txt
$ touch *.txt
$ ls -l *.txt
```

![Tocando todos os txts](../../img/touch_003.png)

A data e hora do ultimo acesso a todos os arquivos .txt na pasta é o mesmo agora.

## Qualquer Caractere

Um outro que nos ajuda bastante é o coringa `?` pois ele vale por __qualquer caractere__ na posição em que ele estiver.

```shell
$ cd pasta1
$ ls -l arq?
```

![Lista completa de arquivos que terminam com qualquer caractere](../../img/wildcards_004.png)

## Procurando por arquivos e diretórios

Outra forma de procurarmos aquivos e diretórios é o comando `find`.  Ele irá percorrer a partir do diretório atual e todos os seus sub diretórios e exibir na tela o caminho. O padrão de pesquisa é informado como uma string.

```shell
$ find . -name "arq?"
```

![Usando o find](../../img/find_001.png)
